import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name  : String,
    last_name   : String,
    email       : String,
    phone       : String
});


export default model('user',UserSchema);



/*
// ====== AJV ======
import Ajv from "ajv";
const ajv = new Ajv();

const schema = {
    type: "object",
    properties: {
        first_name: {type: "string"},
        last_name: {type: "string"},
        email: {type: "string"},
        phone: {type: "string"},
    },
    required: ["first_name", "last_name", "email", "phone"],
    additionalProperties: false

}

const validate = ajv.compile(schema);

const data = {
    first_name: "avital",
    last_name: "vainer",
    email: "avital.va0@gmail.com",
    phone: "0544324210"
}

const isValid = ajv.validate(data);
console.log("is valid? ", isValid);

 */