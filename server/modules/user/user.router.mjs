/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error 
*/
import raw from "../../middleware/route.async.wrapper.mjs";
import user_model from "./user.model.mjs";
import express from 'express';
import log from '@ajar/marker';
import { validateUserData } from "../../middleware/input.validation.mjs";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json())

// CREATES A NEW USER  - without wrapping it with "raw" function
//#region 
// router.post("/", async (req, res,next) => {
//    try{
//      const user = await user_model.create(req.body);
//      res.status(200).json(user);
//    }catch(err){
//       next(err)
//    }
// });
//#endregion

// CREATES A NEW USER
router.post("/", raw( async (req, res) => {
    const isValid = await validateUserData(req.body);

    if(isValid) {
      const user = await user_model.create(req.body);
      res.status(200).json(user);
    }
    else {
      res.status(400).send("User details are invalid");
    }

  })
);

// GET ALL USERS
router.get( "/",raw(async (req, res) => {
    const users = await user_model.find().select(`-_id first_name last_name email phone`);  // same result as: .select(`-_id`);
    res.status(200).json(users);
  })  
);

// GETS A SINGLE USER
router.get("/:id",raw(async (req, res) => {
    const user = await user_model.findById(req.params.id);  // .select(`-_id`);
    if (!user) return res.status(404).json({ status: "No user found." });
    res.status(200).json(user);
  })
);

// UPDATES A SINGLE USER
router.put("/:id",raw(async (req, res) => {
  const isValid = await validateUserData(req.body);
    if(isValid) {
      const user = await user_model.findByIdAndUpdate(req.params.id,req.body, {new: true, upsert: false });
      res.status(200).json(user);
    }
    else {
      res.status(400).send("User details are invalid");
    }
  })
);

// DELETES A USER
router.delete("/:id",raw(async (req, res) => {
    const user = await user_model.findByIdAndRemove(req.params.id);
    if (!user) return res.status(404).json({ status: "No user found." });
    res.status(200).json(user);
  })
);

// PAGINATION
router.get("/paginate/:pageNum/:numOfUsers",raw(async (req, res) => {
    const users = await user_model.find({}).skip(req.params.pageNum * req.params.numOfUsers)
                                .limit(Number(req.params?.numOfUsers));
                                    // .select(`-_id 
                                    //     first_name 
                                    //     last_name 
                                    //     email
                                    //     phone`);
    if (!users) return res.status(404).json({ status: "No user found." });
    res.status(200).json(users);
  })
);



export default router;