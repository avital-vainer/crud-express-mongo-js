import * as yup from "yup";

const schema = yup.object().shape({
    first_name: yup.string().required(),
    last_name: yup.string().required(),
    email: yup.string().email(),
    phone: yup.string().matches(/\d{3}-\d{3}-\d{4}/), // "\d{3}-\d{3}-\d{4}" is a regex of a phone number that looks like this: ###-###-####
});

const data = {
    first_name: "avital",
    last_name: "vainer",
    email: "avital.va0@gmail.com",
    phone: "054-432-4210"
}

export const validateUserData = async (user) => {
    return await schema.isValid(user);
}